provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "my-tf-dsafgdasf-bucket-ac0093-azuredevops"
  acl    = "private"

  tags = {
    Name        = "my-tf-dsafgdasf-bucket-ac0093-azuredevops"
    Environment = "Dev"
  }
}